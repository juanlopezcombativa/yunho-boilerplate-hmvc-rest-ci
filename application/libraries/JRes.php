<?php

/**
 * Libreria para el proceso y respuesta de datos
 * @package jres
 * @version 1.0.0
 * @author Juan López <juanlopez.developer@gmail.com>
 * @license MTE
 */
abstract class JRes {
  
  /**
   * Encapsular respuesta a formato JSON
   * @param int $status Estado
   * @param string $msg Mensaje
   * @param mixed $data Datos
   */
  public function response_json($status, $msg = NULL, $data = NULL) {
    $res = json_encode(array(
        'header' => array(
            'status' => $status
        ),
        'response' => array(
            'msg' => $msg,
            'data' => $data
        ))
    );

    die($res);
  }

  /**
   * Validar método de petición
   * @param string $method POST,GET,PUT,HEAD
   * @return boolean
   */
  public function is_request_method($method) {
    if ($_SERVER['REQUEST_METHOD'] == $method) {
      return true;
    }
  }

}
