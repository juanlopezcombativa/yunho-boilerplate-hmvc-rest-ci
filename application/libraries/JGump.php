<?php

require 'GUMP.php';

/**
 * Extension personalizada de la librería GUMP
 * @package jgump
 * @version 1.0.0
 * @author Juan López <juanlopez.developer@gmail.com>
 * @license MTE
 */
abstract class JGump {
  
  /**
   * Validar datos en el servidor
   * @param mixed $params Parametros
   * @return mixed
   */
  public function validate($params) {
    $valid = new GUMP();
    $inputs = $valid->sanitize($params['data']);
    $valid->validation_rules($params['validation_rules']);
    $valid->filter_rules($params['filter_rules']);
    $input_data = $valid->run($inputs);

    return $input_data;
  }

}