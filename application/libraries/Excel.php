<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

require_once APPPATH . "/third_party/phpexcel/PHPExcel.php";

/**
 * Clase para manejar archivos Excel
 * @package PHPExcel
 * @access public
 * @version 1.0.0
 * @author Juan López <juan@lanaranjamedia.com>
 * @license private
 */
class Excel extends PHPExcel {

  /**
   * Estilos de cabecera del reporte
   * @var array
   */
  var $header_style;

  /**
   * Estilos de cabecera del reporte
   * @var array
   */
  var $cell_style;

  public function __construct() {
    parent::__construct();

    $this->header_style = array(
        'font' => array(
            'bold' => true,
            'size' => 12,
            'name' => 'Calibri',
            'color' => array(
                'rgb' => 'FFFFFF'
            )
        ),
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
        )
    );

    $this->cell_style = array(
        'font' => array(
            'bold' => true,
            'size' => 10,
            'name' => 'Calibri',
            'color' => array(
                'rgb' => '000000'
            )
        ),
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );
  }

  /**
   * Generar excel: Reporte de usuarios con sus historias
   * @param array $data Usuarios
   * @param string $author Autor del reporte
   * @return string Nombre del archivo
   */
  public function report_user_their_stories($data = '', $author = '') {

    $author = ($author == '') ? 'Api' : $author;

    $objPHPExcel = new PHPExcel();

    // Propiedades Documento
    $objPHPExcel->getProperties()->setCreator('Admin | API Monopolio');
    $objPHPExcel->getProperties()->setLastModifiedBy('Admin | API Monopolio');
    $objPHPExcel->getProperties()->setTitle('Reporte de Usuarios');
    $objPHPExcel->getProperties()->setSubject('Reporte de Usuarios');
    $objPHPExcel->getProperties()->setDescription('Lista de usuarios con sus historias');

    // Estilo de cabecera
    $header_style = $this->header_style;

    // Estilo para celdas principales
    $cell_style = $this->cell_style;

    $objPHPExcel->setActiveSheetIndex(0);

    // Detalle
    $objPHPExcel->getActiveSheet()->getStyle('A:T')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getStyle('A:T')->getFont()->setSize(10);

    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Reportes - Usuarios');
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18);
    $objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
    $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Aplicación: API Monopoly');
    $objPHPExcel->getActiveSheet()->mergeCells('A2:C2');
    $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Generado el: ' . date('d/m/Y H:i:s'));
    $objPHPExcel->getActiveSheet()->mergeCells('A3:C3');

    // Cabecera
    $objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(30);
    $objPHPExcel->getActiveSheet()->getStyle('A5:P5')->applyFromArray($header_style);
    $objPHPExcel->getActiveSheet()->getStyle('A5:P5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A5:P5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('A5:P5')->getFill()->getStartColor()->setRGB('fb963f');

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
    $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Nro.');

    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(11);
    $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Usuario ID');

    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
    $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Facebook');

    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
    $objPHPExcel->getActiveSheet()->setCellValue('D5', 'DNI');

    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
    $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Nombre');

    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
    $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Apellido');

    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(14);
    $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Teléfono');

    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
    $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Email');

    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
    $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Tiene apoderado');

    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
    $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Apoderado Nomnbre');

    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
    $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Apoderado Apellido');

    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
    $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Apoderado Email');

    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(23);
    $objPHPExcel->getActiveSheet()->setCellValue('M5', 'Fecha Registro Usuario');

    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(11);
    $objPHPExcel->getActiveSheet()->setCellValue('N5', 'Historia ID');

    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(30);
    $objPHPExcel->getActiveSheet()->setCellValue('O5', 'Historia');

    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(23);
    $objPHPExcel->getActiveSheet()->setCellValue('P5', 'Fecha Registro Comentario');

    $row = 6; // Fila de inicio
    $count = 0;

    foreach ($data as $key => $item) {
      $count ++;

      // Altura de filas y colores
      $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':' . 'P' . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

      // Nro *
      $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $count);

      // Usuario ID * 
      $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $item['user_id']);

      // Facebook * 
      $url = 'http://www.facebook.com/' . $item['user_fbid'];
      $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $url);
      $objPHPExcel->getActiveSheet()
              ->getCell('C' . $row)
              ->getHyperlink()
              ->setUrl($url);

      // DNI *
      $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $item['user_dni']);

      // Nombre *
      $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $item['user_name']);

      // Apellido *
      $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $item['user_lastname']);

      // Teléfono *
      $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $item['user_phone']);

      // Email *
      $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $item['user_email']);

      // Menor de edad *
      $minor_state = ($item['user_attorney_state'] == 1) ? 'Si' : 'No';
      $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $minor_state);

      // Menor Nombre *
      $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $item['user_attorney_name']);

      // Menor Apellido *
      $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $item['user_attorney_lastname']);

      // Menor Email *
      $objPHPExcel->getActiveSheet()->setCellValue('L' . $row, $item['user_attorney_email']);

      // Fecha de registro de usuario *
      $objPHPExcel->getActiveSheet()->setCellValue('M' . $row, $item['user_daterecord']);

      // Historia ID *
      $objPHPExcel->getActiveSheet()->setCellValue('N' . $row, $item['comment_id']);

      // Historio Descripción
      $objPHPExcel->getActiveSheet()->setCellValue('O' . $row, $item['comment_description']);
      $objPHPExcel->getActiveSheet()->getStyle('O' . $row)->getAlignment()->setWrapText(true);

      // Fecha de registro de historia
      $objPHPExcel->getActiveSheet()->setCellValue('P' . $row, $item['comment_daterecord']);

      $row++;
    }

    $objPHPExcel->setActiveSheetIndex(0);

    //Crear Excel
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

    //Generar nombre y guardar en la carpeta 'files/report'
    $hash = hash('adler32', microtime() * rand(0, 10));
    $file_name = date('d-m-Y') . '_report-users_' . $hash;
    $file = PATH_FILES . DS . 'report' . DS . $file_name . '.php';

    $objWriter->save(str_replace('.php', '.xlsx', $file));

    return $file_name . '.xlsx';
  }

}
