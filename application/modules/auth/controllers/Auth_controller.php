<?php

/**
 * Controlador Autenticación
 * 
 * @package myapp
 * @subpackage myapp-auth
 * @category controller
 * @author Juan López <juanlopez.developer@gmail.com>
 * @property User_model $User_model Modelo Usuario
 * @property Systoken_model $Systoken_model Modelo Token
 */
class Auth_controller extends MX_Controller {

  public function __construct() {
    parent::__construct();
  }

  /**
   * Autenticación de usuario al sistema
   */
  public function login() {
    if ($this->input->method() == 'post') {

      /* Validar parametros */
      $input_data = JGump::validate(array(
                  'data' => $this->input->post(),
                  'validation_rules' => array(
                      'user_account' => 'required',
                      'user_password' => 'required'
                  ),
                  'filter_rules' => array(
                      'user_account' => 'trim|sanitize_string',
                      'user_password' => 'trim|sanitize_string'
                  )
      ));

      if ($input_data) {
        $this->load->model('User_model');

        /* Buscar usuario por cuenta y contraseña */
        $user = $this->User_model->get_by_credentials(
                $input_data['user_account'], $input_data['user_password']
        );

        if ($user) {
          
          /* Generar y guardar Token de usuario */
          $token = $this->save_token($user['user_id']);

          /* Guardar en sesión */
          $session_data = array(
              'user' => $user,
              'token_code' => $token,
              'logged_in' => TRUE
          );

          $this->session->set_userdata($session_data);

          JRes::response_json(STATUS_SUCCESS, 'Autenticación correcta');
        } else {
          JRes::response_json(STATUS_FAILED, 'Cuenta y/o contraseña incorrecta');
        }
      } else {
        JRes::response_json(STATUS_FAILED, 'Cuenta y contraseña no ingresados');
      }
    }
  }

  /**
   * Generar un codigo de acceso
   * @return string
   */
  private function generate_token_code() {
    $token = md5(uniqid());
    return $token;
  }

  /**
   * Guardar token de acceso de un usuario
   * @param int $user_id ID Usuario
   * @return array Token de usuario
   */
  private function save_token($user_id) {
    $token_code = $this->generate_token_access();
    $token = NULL;

    $this->load->model('Systoken_model');

    $systoken_data = array(
        'user_id' => $user_id,
        'systoken_code' => $token_code,
        'systoken_dateexpiry' => NULL,
        'systoken_state' => 1,
    );
    $token_id = $this->Systoken_model->insert($systoken_data);

    if ($token_id > 0) {
      $token = $systoken_data;
    }

    return $token;
  }

}
