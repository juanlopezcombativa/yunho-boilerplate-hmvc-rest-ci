<?php

/**
 * Extender de REST_Controller
 */
require(APPPATH . '/libraries/REST_Controller.php');

/**
 * Controlador de Usuario
 * 
 * @package CodeIgniter
 * @subpackage Api
 * @category Controller
 * @author Juan López <juanlopez.developer@gmail.com>
 * @property User_model $User_model Modelo Usuario
 * @property Comment_model $Comment_model Modelo Comentario
 */
class User extends REST_Controller {

  public function __construct() {
    parent::__construct();

    $this->load->model('User_model');
  }

  /**
   * Autenticar usuario
   */
  public function login_post() {
    // Validacion en servidor
    $objGump = new GUMP();
    $inputs = $objGump->sanitize($this->post());

    $objGump->validation_rules(array(
        'login_type' => 'required|max_len,3',
        'user_fbid' => 'max_len,24',
        'user_dni' => 'max_len,11'
    ));

    $objGump->filter_rules(array(
        'login_type' => 'trim|sanitize_string',
        'user_fbid' => 'trim|sanitize_string',
        'user_dni' => 'trim|sanitize_string'
    ));

    $input_data = $objGump->run($inputs);

    // Validar Parametros
    if (!$input_data) {
      $this->response(wrap_data(STATUS_FAILED, NULL, MSG_RESPONSE_PARAMS_FAILED));
    }

    // Autenticar
    $login_type = $input_data['login_type'];
    $user_data = array();
    switch ($login_type) {
      case 'fb':
        $user_data = $this->User_model->get_by_fbid($input_data['user_fbid']);
        break;
      case 'dni':
        $user_data = $this->User_model->get_by_dni($input_data['user_dni']);
        break;
      default:
        $this->response(wrap_data(STATUS_FAILED, NULL, 'Tipo de autenticación no permitida'));
        break;
    }

    if ($user_data) {
      // Ok
      $this->response(wrap_data(STATUS_SUCCESS, $user_data['user_dni'], 'Autenticación correcta'));
    } else {
      // Fallido
      $this->response(wrap_data(STATUS_FAILED, NULL, 'Usuario no registrado'));
    }
  }

  /**
   * Insertar un nuevo usuario
   */
  public function add_post() {

    // Validacion en servidor
    $objGump = new GUMP();
    $inputs = $objGump->sanitize($this->post());

    $objGump->validation_rules(array(
        'user_fbid' => 'max_len,24',
        'user_dni' => 'required|max_len,11',
        'user_name' => 'required|max_len,160',
        'user_lastname' => 'required|max_len,160',
        'user_phone' => 'required|max_len,160',
        'user_email' => 'required|max_len,160',
        'user_attorney_state' => 'numeric',
        'user_attorney_name' => 'max_len,160',
        'user_attorney_lastname' => 'max_len,160',
        'user_attorney_email' => 'max_len,160'
    ));

    $objGump->filter_rules(array(
        'user_fbid' => 'trim|sanitize_string',
        'user_dni' => 'trim|sanitize_string',
        'user_name' => 'trim|sanitize_string',
        'user_lastname' => 'trim|sanitize_string',
        'user_phone' => 'trim|sanitize_string',
        'user_email' => 'trim|sanitize_email',
        'user_attorney_state' => 'trim|sanitize_numbers',
        'user_attorney_name' => 'trim|sanitize_string',
        'user_attorney_lastname' => 'trim|sanitize_string',
        'user_attorney_email' => 'trim|sanitize_email'
    ));

    $input_data = $objGump->run($inputs);

    // Validar Parametros
    if (!$input_data) {
      $this->response(wrap_data(STATUS_FAILED, NULL, MSG_RESPONSE_PARAMS_FAILED));
    }

    // Validar DNI
    $user_dni = $input_data['user_dni'];
    $exist_dni = $this->User_model->exist_dni($user_dni);
    if ($exist_dni) {
      $this->response(wrap_data(STATUS_FAILED, NULL, 'El DNI "' . $user_dni . '" ya se encuentra registrado'));
    }

    // Parametros
    $user_params = array(
        'user_fbid' => $input_data['user_fbid'],
        'user_dni' => $input_data['user_dni'],
        'user_name' => $input_data['user_name'],
        'user_lastname' => $input_data['user_lastname'],
        'user_phone' => $input_data['user_phone'],
        'user_email' => $input_data['user_email'],
        'user_attorney_state' => $input_data['user_attorney_state'],
        'user_attorney_name' => $input_data['user_attorney_name'],
        'user_attorney_lastname' => $input_data['user_attorney_lastname'],
        'user_attorney_email' => $input_data['user_attorney_email'],
        'user_state' => 1,
        'user_daterecord' => date('Y-m-d H:i:s')
    );

    // Guardar
    $user_id = $this->User_model->insert($user_params);

    if ($user_id > 0) {
      // Ok
      $this->response(wrap_data(STATUS_SUCCESS, $user_dni, MSG_RESPONSE_INSERT_SUCCESS));
    } else {
      // Fallido
      $this->response(wrap_data(STATUS_FAILED, NULL, MSG_RESPONSE_INSERT_FAILED));
    }
  }

  /**
   * Insertar un comentario de un usuario
   */
  public function add_comment_post() {

    // Validacion en servidor
    $objGump = new GUMP();
    $inputs = $objGump->sanitize($this->post());

    $objGump->validation_rules(array(
        'user_dni' => 'required|max_len,11',
        'comment_description' => 'required'
    ));

    $objGump->filter_rules(array(
        'user_dni' => 'trim|sanitize_string',
        'comment_description' => 'trim|sanitize_string'
    ));

    $input_data = $objGump->run($inputs);

    // Validar Parametros
    if (!$input_data) {
      $this->response(wrap_data(STATUS_FAILED, NULL, MSG_RESPONSE_PARAMS_FAILED));
    }

    // Validar Usuario
    $user_data = $this->User_model->get_by_dni($input_data['user_dni']);
    if (!$user_data) {
      $this->response(wrap_data(STATUS_FAILED, NULL, 'El usuario no existe'));
    }

    // Modelo 'Comentario'
    $this->load->model('Comment_model');

    // Parametros
    $comment_params = array(
        'user_id' => $user_data['user_id'],
        'comment_description' => $input_data['comment_description'],
        'comment_state' => 1,
        'comment_daterecord' => date('Y-m-d H:i:s')
    );

    // Guardar
    $comment_id = $this->Comment_model->insert($comment_params);

    if ($comment_id > 0) {
      // Ok
      $this->response(wrap_data(STATUS_SUCCESS, $comment_id, MSG_RESPONSE_INSERT_SUCCESS));
    } else {
      // Fallido
      $this->response(wrap_data(STATUS_FAILED, NULL, MSG_RESPONSE_INSERT_FAILED));
    }
  }

  /**
   * Lista datos de un usuario 
   * @param int $id ID Usuario
   */
  public function detail_get() {
    if (!$this->get('id')) {
      $this->response(wrap_data(STATUS_FAILED, MSG_RESPONSE_PARAMS_FAILED));
    }

    $user = $this->User_model->get($this->get('id'));

    if ($user) {
      $this->response(wrap_data(STATUS_SUCCESS, $user, MSG_RESPONSE_GET_SUCCESS));
    } else {
      $this->response(wrap_data(STATUS_FAILED, NULL, MSG_RESPONSE_GET_FAILED));
    }
  }

  /**
   * Listar todos los usuarios
   */
  public function list_get() {
    $data = $this->User_model->get_all();

    if ($data) {
      $this->response(wrap_data(STATUS_SUCCESS, $data, MSG_RESPONSE_GET_SUCCESS));
    } else {
      $this->response(wrap_data(STATUS_FAILED, NULL, MSG_RESPONSE_GET_FAILED));
    }
  }

  /**
   * Exportar excel lista de usuarios con sus historias(comentarios)
   */
  public function export_stories_get() {
    $data = $this->User_model->get_all_their_stories();

    if ($data) {
      $this->load->library('Excel');
      $objExcel = new Excel();

      $file = $objExcel->report_user_their_stories($data);
      $fichero = PATH_FILES . DS . 'report' . DS . $file;

      if (file_exists($fichero)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $file . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($fichero));
        readfile($fichero);
        exit;
      } else {
        $this->response(wrap_data(STATUS_FAILED, NULL, 'Ha ocurrido un error, intente nuevamente'));
      }
    } else {
      $this->response(wrap_data(STATUS_FAILED, NULL, MSG_RESPONSE_GET_FAILED));
    }
  }

}
