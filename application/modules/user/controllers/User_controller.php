<?php

/**
 * Controlador para agregar usuarios
 * 
 * @package myapp
 * @subpackage myapp-user
 * @category controller
 * @author Juan López <juanlopez.developer@gmail.com>
 * @property User_model $User_model Modelo Usuario
 */
class User_controller extends MX_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('User_model');
  }

  /**
   * Autenticar usuario
   */
  public function login() {
    /** module and controller names are different, you must include the method name also, including 'index' * */
    echo modules::run('auth/Auth_controller/login');
  }
  
  

}
