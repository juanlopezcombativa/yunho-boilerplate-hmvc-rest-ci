<?php

/**
 * Mensajes de aplicación
 * @param string $type
 * @return string
 */
function app_msg($type) {
  $msg = array (
      'RESPONSE_INSERT_SUCCESS' => 'Registro guardado correctamente.',
      'RESPONSE_INSERT_FAILED' => 'No se pudo guardar el registro.',
      'RESPONSE_UPDATE_SUCCESS' => 'Registro actualizado correctamente.',
      'RESPONSE_UPDATE_FAILED' => 'No se pudo actualizar el registro.',
      'RESPONSE_DELETE_SUCCESS' => 'Registro eliminado correctamente.',
      'RESPONSE_DELETE_FAILED' => 'No se pudo eliminar el registro.',
      'RESPONSE_PARAMS_FAILED' => 'Los campos obligatorios estan vacíos o erróneos',
      'RESPONSE_SERVER_FAILED' => 'Ha ocurrido un error en el sistema, porfavor intentelo mas tarde.',
      'RESPONSE_PERMISSION_DENIED' => 'No tiene permisos para realizar esta petición.',
      'RESPONSE_SESSION_EXPIRED' => 'Su sesión ha expirado, vuelva a iniciar sesión',
      'RESPONSE_LIST_SUCCESS' => 'Lista cargada correctamente',
      'RESPONSE_LIST_FAILED' => 'No hay datos para mostrar en la lista',
  );
  
  return $msg[$type];
}
