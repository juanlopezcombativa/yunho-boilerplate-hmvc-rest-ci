<?php

/**
 * Encapsula/Envolver los datos de respuesta en un array
 * @param int $status
 * @param mixed $data
 * @param string $msg
 * @return array
 */
function wrap_data($status, $data = NULL, $msg = "") {
  return array(
      'response_status' => $status,
      'response_data' => $data,
      'response_msg' => $msg
  );
}
