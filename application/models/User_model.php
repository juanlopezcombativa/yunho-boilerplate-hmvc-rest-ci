<?php

/**
 * Modelo Usuario
 * 
 * @package myapp
 * @subpackage myapp-model
 * @category model
 * @author Juan López <juan@lanaranjamedia.com>
 */
class User_model extends MY_Model {

  /**
   * Nombre de la tabla
   * @var string
   */
  protected $table_name = 'tbl_user';

  public function __construct() {
    parent::__construct();
  }

  /**
   * <Master> Función principal para obtner un usuario
   * @param string $type Tipo de consulta
   * @param array $params Filtros
   */
  public function get_one_by_params($type = '', $params = null) {
    $this_table = $this->table_name;
    $this->db->select('*');
    $this->db->from($this_table);

    if ($type == 'ID') {
      /* Buscar usuario por su id */
      $this->db->where($this_table . '.user_id', $params['user_id']);
    } elseif ($type == 'CREDENTIALS') {
      /* Buscar usuario por su cuenta y contraseña */
      $this->db->where($this_table . '.user_account', $params['user_account']);
      $this->db->where($this_table . '.user_password', $params['user_password']);
    }

    $this->db->limit(1);

    $query = $this->db->get();
    return $query->row_array();
  }

  /**
   * Obtener un usuario
   * @param int $user_id ID Usuario
   * @return array Usuario
   */
  public function get_one($user_id) {
    $params['user_id'] = $user_id;
    return $this->get_one_by_params('ID', $params);
  }
  
  /**
   * Obtener usuario por sus credenciales
   * @param string $user_account Cuenta
   * @param string $user_password Contraseña
   * @return array Usuario
   */
  public function get_by_credentials($user_account, $user_password) {
    $params['user_account'] = $user_account;
    $params['user_password'] = $user_password;
    return $this->get_one_by_params('CREDENTIALS', $params);
  }

}
