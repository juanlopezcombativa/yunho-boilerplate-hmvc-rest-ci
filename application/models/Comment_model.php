<?php

/**
 * Modelo Comentario
 * 
 * @package myapp
 * @subpackage myapp-model
 * @category model
 * @author Juan López <juan@lanaranjamedia.com>
 */
class Comment_model extends MY_Model {

  /**
   * Nombre de la tabla
   * @var string
   */
  protected $table_name = 'tmo_comment';

  public function __construct() {
    parent::__construct();
  }

  /**
   * Obtener un comentario
   * @access public
   * @param int $comment_id ID Comentario
   * @return array Usuario
   */
  public function get($comment_id) {
    $this->db->select('*')
            ->from($this->table_name)
            ->where('comment_state', 1)
            ->where('comment_id', $comment_id)
            ->limit(1);

    $query = $this->db->get();

    return $query->row_array();
  }

  /**
   * Obtener lista de todos los comentarios
   * @return array Lista
   */
  public function get_all() {
    $this->db->select('*')
            ->from($this->table_name)
            ->where('comment_state', 1);

    $query = $this->db->get();

    return $query->result_array();
  }

  /**
   * Obtener lista de todos los comentarios de un usuario
   * @param int $user_id ID Usuario
   * @return array Lista
   */
  public function get_all_by_user($user_id) {
    $this->db->select('*')
            ->from($this->table_name)
            ->where('comment_state', 1)
            ->where('user_id', $user_id);

    $query = $this->db->get();

    return $query->result_array();
  }

}
