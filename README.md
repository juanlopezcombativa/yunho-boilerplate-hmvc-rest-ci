## CODEIGNITER 3 BOILERPLATE

### What's included:

* Codeigniter 3.0: [http://codeigniter.com](http://codeigniter.com)
* Modular Extensions: [https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc](https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc)
* php-activerecord: [http://www.phpactiverecord.org/](http://www.phpactiverecord.org/)
* CodeIgniter-Ion-Auth: [https://github.com/benedmunds/CodeIgniter-Ion-Auth](https://github.com/benedmunds/CodeIgniter-Ion-Auth)
* dompdf: [https://dompdf.github.io/](https://dompdf.github.io/)
* PHPExcel: [http://phpexcel.codeplex.com/](http://phpexcel.codeplex.com/)

## API REST EXAMPLE

### URL'S:

* Login: .../api/user/login/x-api-key/8hu8fWMCIhCXyq0U4TP0CMJ9waHkCGNcsrqok8zS
* Add User: .../api/user/add/x-api-key/8hu8fWMCIhCXyq0U4TP0CMJ9waHkCGNcsrqok8zS
* Add Comment: .../api/user/add_comment/x-api-key/8hu8fWMCIhCXyq0U4TP0CMJ9waHkCGNcsrqok8zS
* Export User Stories: .../api/user/export_stories/x-api-key/8hu8fWMCIhCXyq0U4TP0CMJ9waHkCGNcsrqok8zS

### FUNCTIONS:

#### LOGIN
* Url: /api/user/login/x-api-key/8hu8fWMCIhCXyq0U4TP0CMJ9waHkCGNcsrqok8zS
* Method: POST
* Params: 
    * login_type (string)
        * "fb" Facebook
        * "dni" DNI
    * user_fbid (string)
    * user_dni (string)
* Return: JSON
    * response_status: 1(Success) or 0(Failed)
    * response_data: DNI User OR NULL
    * response_msg: Message

#### ADD USER
* Url: /api/user/add/x-api-key/8hu8fWMCIhCXyq0U4TP0CMJ9waHkCGNcsrqok8zS
* Method: POST
* Params: 
    * user_fbid (string)
    * user_dni (string)
    * user_name (string)
    * user_lastname (string)
    * user_phone (string)
    * user_email (string)
    * user_attorney_state (int) (not required)
        * 1: exist attorney
        * 0: no exist attorney
        * default: 0
    * user_attorney_name (string) (not required)
    * user_attorney_lastname (string) (not required)
    * user_attorney_email (string) (not required)
* Return: JSON
    * response_status: 1(Success) or 0(Failed)
    * response_data: DNI User or NULL
    * response_msg: Message

#### ADD COMMENT
* Url: /api/user/add_comment/x-api-key/8hu8fWMCIhCXyq0U4TP0CMJ9waHkCGNcsrqok8zS
* Method: POST
* Params: 
    * user_dni (string)
    * comment_description (string)
* Return: JSON
    * response_status: 1(Success) or 0(Failed)
    * response_data: ID Comment or NULL
    * response_msg: Message

#### EXPORT TO EXCEL USERS WITH STORIES
* Url: /api/user/export_stories/x-api-key/8hu8fWMCIhCXyq0U4TP0CMJ9waHkCGNcsrqok8zS
* Method: GET
* Return: Force download excel file


## CREDITS / REFERENCE:
* zentraedi [https://github.com/zentraedi/codeigniter3-boilerplate](https://github.com/zentraedi/codeigniter3-boilerplate)
* Unodepiera [https://uno-de-piera.com/proyecto-base-codeigniter3-hmvc-rest-ion-auth/](https://uno-de-piera.com/proyecto-base-codeigniter3-hmvc-rest-ion-auth/)
